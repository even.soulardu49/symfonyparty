<?php

namespace App\Form;

use App\Entity\Group;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AllPaymentAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paymentInfo', PaymentAddInfoType::class)
            ->add('includeUserInPayment', ChoiceType::class, [
                'choices'  => [
                    'Oui' => true,
                    'Non' => false,
                ],
            ])
            ->add('users', EntityType::class, [
                'class'=>User::class, //choix de la classe à afficher
                'choices' => $options['group']->getUsersWithoutTheActual($options['user']),
                'choice_label'=>'email', //quel champ de cette classe utiliser pour le libellé
                'multiple'=>true, //ici non parce qu'on est en ManyToOne
                'expanded'=>true, //génère un select si false, des boutons radios si true
                'label'=>false,
            ])
            ->add("create",SubmitType::class, ["label"=>"Créer les paiements"])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'group'=> Group::class,
            'user'=>User::class,
        ]);
    }
}
