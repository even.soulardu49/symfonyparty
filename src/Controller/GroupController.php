<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Payment;
use App\Entity\User;
use App\Form\AllGroupAddType;
use App\Form\AllGroupEditType;
use App\Form\GroupAddType;
use App\Form\LeaveGroupType;
use App\Form\RemoveGroupType;
use App\Form\UserResearchType;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class GroupController extends AbstractController
{
    /**
     * @Route("/group/addGroup", name="addGroup")
     */
    public function addGroup(Request $request, UserRepository $repo)
    {


        $group = new Group();

        $form = $this->createForm(AllGroupAddType::class);
        $form->handleRequest($request);

        $userList = null;

        if($form->getClickedButton() === $form->get('research')){
            $session = $request->getSession();

            $session->set('title', $form->getData()["groupAdd"]->getTitle());


            $user = $form->getData();
            $research = $user["userResearch"]["research"];
            if($research != null){
                $userList = $repo->search($user["userResearch"]["research"]);
                if ($userList == null) {
                    $this->addFlash('erreur', 'Aucun utilisateur contenant possédant ce nom ou ce prénom n\'a été trouvé, essayez en un autre.');
                }
            }
        }

        if ($form->getClickedButton() === $form->get('add')) {
            $formData = $form->getData();

            if($formData["groupAdd"]->getTitle() != null){
                $group->setTitle($formData["groupAdd"]->getTitle());

                $session = $request->getSession();
                $sessionVal = $session->get('userEmails');

                $uRepository = $this->getDoctrine()->getRepository(User::class);

                $user=$uRepository->find($this->getUser()->getId());
                $group->addAssoUserGroup($user);

                if($sessionVal != null){
                    foreach($sessionVal as $val){
                        $user=$uRepository->findOneBy(['email' => $val]);
                        $group->addAssoUserGroup($user);
                    }
                }

                $session->clear();
                //récupérer l'entity manager (objet qui gère la connection à la bdd)
                $em= $this->getDoctrine()->getManager();

                //je dis au manager que je veux garder l'objet en BDD
                $em->persist($group);

                //je déclenche l'insert
                $em->flush();

                return $this->redirectToRoute("detailGroup", array("id"=>$group->getId()));
            }else{
                $this->addFlash('erreur', 'Veuillez renseigner le titre du groupe');
            }
        }

        return $this->render("group/add.html.twig", [
            "users"=>$userList,
            "addForm"=>$form->createView(),
        ]);
    }

    /**
     * @Route("/group/addUserToAGroup/${userEmail}/${previousPage}/${id}", name="addUserToAGroup")
     */
    public function addUserToAGroup($userEmail, Request $request, $previousPage, $id)
    {
        $session = $request->getSession();

        $sessionEmails = $session->get('userEmails');

        if($sessionEmails != null){
            $canAdd = true;
            foreach ($sessionEmails as $val){
                if($val == $userEmail){
                    $canAdd = false;
                }
            }
            if($canAdd){
                array_push($sessionEmails, $userEmail);
                $session->set('userEmails', $sessionEmails);
            }
        }else{
            $session->start();
            $session->set('userEmails', array($userEmail));
        }

        if($previousPage=='edit') {
            return $this->redirectToRoute("groupEdit", ["id"=>$id]);
        }
        else {
            return $this->redirectToRoute("addGroup");
        }
    }


    /**
     * @Route("/group/removeUserToAGroup/${userEmail}/${previousPage}/${id}", name="removeUserToAGroup")
     */
    public function removeUserToAGroup($userEmail, Request $request, $previousPage, $id)
    {
        $session = $request->getSession();

        $sessionVal = $session->get('userEmails');
        if($sessionVal != null){
            array_splice($sessionVal, array_search($userEmail,$sessionVal), 1);
            $session->set('userEmails', $sessionVal);
        }


        if($previousPage=='edit') {
            $em= $this->getDoctrine()->getManager();
            $groupRepo=$this->getDoctrine()->getRepository(Group::class);
            $group=$groupRepo->find($id);
            foreach($group->getAssoUserGroup() as $user){
                if($user->getEmail() == $userEmail){
                    $group->removeAssoUserGroup($user);
                    $em->persist($group);
                    $em->flush();
                }
            }


            return $this->redirectToRoute("groupEdit", ["id"=>$id]);
        }
        else {
            return $this->redirectToRoute("addGroup");
        }
    }

    /**
     * @Route("/group/editGroup/{id}", name="groupEdit")
     */
    public function groupEdit($id, Request $request, UserRepository $repo)
    {

        $groupRepo=$this->getDoctrine()->getRepository(Group::class);
        $group=$groupRepo->find($id);

        foreach($group->getAssoUserGroup() as $user){
            $this->addUserToAGroup($user->getEmail(), $request, "edit",$id);
        }

        $form = $this->createForm(AllGroupEditType::class);
        $form->handleRequest($request);

        $userList = null;

        if($form->getClickedButton() === $form->get('research')){
            $session = $request->getSession();

            $session->set('title', $form->getData()["groupAdd"]->getTitle());


            $user = $form->getData();
            $research = $user["userResearch"]["research"];
            if($research != null){
                $userList = $repo->search($user["userResearch"]["research"]);
                if ($userList == null) {
                    $this->addFlash('erreur', 'Aucun utilisateur contenant possédant ce nom ou ce prénom n\'a été trouvé, essayez en un autre.');
                }
            }
        }

        if ($form->getClickedButton() === $form->get('edit')) {
            $formData = $form->getData();

            if($formData["groupAdd"]->getTitle() != null){
                $group->setTitle($formData["groupAdd"]->getTitle());

                $session = $request->getSession();
                $sessionVal = $session->get('userEmails');

                $uRepository = $this->getDoctrine()->getRepository(User::class);

                $user=$uRepository->find($this->getUser()->getId());
                $group->addAssoUserGroup($user);

                if($sessionVal != null){
                    foreach($sessionVal as $val){
                        $user=$uRepository->findOneBy(['email' => $val]);
                        $group->addAssoUserGroup($user);
                    }
                }

                $session->clear();
                //récupérer l'entity manager (objet qui gère la connection à la bdd)
                $em= $this->getDoctrine()->getManager();

                //je dis au manager que je veux garder l'objet en BDD
                $em->persist($group);

                //je déclenche l'insert
                $em->flush();

                return $this->redirectToRoute("detailGroup", array("id"=>$group->getId()));
            }else{
                $this->addFlash('erreur', 'Veuillez renseigner le titre du groupe');
            }
        }

        return $this->render("group/edit.html.twig", [
            "users"=>$userList,
            "editForm"=>$form->createView(),
            "group"=>$group,
        ]);
    } 

    /**
     * @Route("/group/detail/${id}", name="detailGroup")
     */
    public function getDetail($id)
    {
        $repository = $this->getDoctrine()->getRepository(Group::class);

        $group=$repository->find($id);

        return $this->render("group/detail.html.twig", [
            "group"=>$group,
        ]);
    }

    /**
     * @Route("/group/remove/${groupId}", name="removeGroup")
     */
    public function remove($groupId, Request $request){
        //récupérer le chaton
        $repo=$this->getDoctrine()->getRepository(Group::class);
        $group=$repo->find($groupId);

        //créer le formulaire
        $form = $this->createForm(RemoveGroupType::class, $group);

        //gérer le retour du POST
        $form->handleRequest($request);

        $payments = $group->getPayments();
        if ($form->isSubmitted() && $form->isValid()){
            //récupérer l'entity manager (objet qui gère la connection à la BDD)
            $em=$this->getDoctrine()->getManager();

            //je dis au manager que je veux garde l'objet en BDD
            foreach ($payments as $payment) {
                $em->remove($payment);
            }

            $em->remove($group);

            //je déclenche l'insert
            $em->flush();

            return $this->redirectToRoute("app_home");
        }

        return $this->render("group/remove.html.twig",[
            "formRemove"=>$form->createView(),
            "group"=>$group,
        ]);
    }

    /**
     * @Route("/group/leave/${groupId}/${userId}", name="leaveGroup")
     */
    public function leave($groupId, $userId, Request $request){
        //récupérer le chaton
        $repo=$this->getDoctrine()->getRepository(Group::class);
        $group=$repo->find($groupId);

        $repo=$this->getDoctrine()->getRepository(User::class);
        $user=$repo->find($userId);

        //créer le formulaire
        $form = $this->createForm(LeaveGroupType::class, $group);

        //gérer le retour du POST
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $group->removeAssoUserGroup($user);

            //récupérer l'entity manager (objet qui gère la connection à la BDD)
            $em=$this->getDoctrine()->getManager();

            //je dis au manager que je veux garde l'objet en BDD
            $em->persist($group);

            //je déclenche l'insert
            $em->flush();

            return $this->redirectToRoute("app_home");
        }

        return $this->render("group/leave.html.twig",[
            "formLeave"=>$form->createView(),
            "group"=>$group,
        ]);
    }

}


