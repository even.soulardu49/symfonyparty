<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Payment;
use App\Entity\User;
use App\Form\AllGroupAddType;
use App\Form\AllPaymentAddType;
use App\Form\BetaPaymentAddType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends AbstractController
{

    /**
     * @Route("/payment/add/${groupId}", name="addPayment")
     */
    public function ajouter(Request $request, $groupId)
    {

        $gRepository = $this->getDoctrine()->getRepository(Group::class);
        $group =$gRepository->find($groupId);

        $form=$this->createForm(AllPaymentAddType::class, null, array(
            'group' => $group,
            'user' => $this->getUser(),
        ));

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $formData = $form->getData();

            foreach ($formData["users"] as $user){
                $payment = new Payment();

                $payment->setTitle($formData["paymentInfo"]->getTitle());

                if($formData["includeUserInPayment"]){
                    $nbUserWhoHaveToPay = count($formData["users"] ) + 1;
                }else{
                    $nbUserWhoHaveToPay = count($formData["users"] );
                }


                $priceForEachUser = round($formData["paymentInfo"]->getAmount()/$nbUserWhoHaveToPay, 2);
                $payment->setAmount($priceForEachUser);

                $time = new \DateTime();
                $time->format('d/m/y');
                $payment->setDate($time);

                $payment->setGroupId($group);

                $payment->setIsPaid(false);

                $uRepository = $this->getDoctrine()->getRepository(User::class);

                $userWhoPaid =$uRepository->find($this->getUser()->getId());
                $payment->setUserWhoPaid($userWhoPaid);

                //récupérer l'entity manager (objet qui gère la connection à la bdd)
                $em= $this->getDoctrine()->getManager();

                $userClass =$uRepository->find($user->getId());
                $payment->setUserWhoNeedToPay($userClass);
                //je dis au manager que je veux garder l'objet en BDD
                $em->persist($payment);
                //je déclenche l'insert
                $em->flush();
            }

            return $this->redirectToRoute("detailGroup",  ["id"=>$groupId]);
        }
        return $this->render("payment/add.html.twig", [
            "addForm"=>$form->createView(),
        ]);
    }

    /**
     * @Route("/payment/refundPayment/${previousPage}/${groupId}/${paymentId}", name="refundPayment")
     */
    public function refundPayment($previousPage, $groupId, $paymentId)
    {
        $pRepo = $this->getDoctrine()->getRepository(Payment::class);
        $payment = $pRepo->find($paymentId);

        $payment->setIsPaid(true);

        $em= $this->getDoctrine()->getManager();

        $em->persist($payment);
        //je déclenche l'insert
        $em->flush();

        if($previousPage == "detail"){
            return $this->redirectToRoute("detailGroup",  ["id"=>$groupId]);
        }else{
            return $this->redirectToRoute("app_home");
        }

    }
}
