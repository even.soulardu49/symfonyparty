<?php

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupAddType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="/")
     * @Route("/home", name="app_home")
     */
    public function index()
    {
        if($this->isGranted("IS_AUTHENTICATED_FULLY")){

            //récupérer le repository
            $repository = $this->getDoctrine()->getRepository(Group::class);
            //je lis la bdd
            $group = $repository->findAll();

            return $this->render('home/home.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }else{
            return $this->redirectToRoute('app_login');
        }
    }

    /**
     * @Route("/home/refundPayment/${previousPage}/${groupId}/${paymentId}", name="refundPayment")
     */
    public function refundPayment($previousPage, $groupId, $paymentId)
    {
        $pRepo = $this->getDoctrine()->getRepository(Payment::class);
        $payment = $pRepo->find($paymentId);

        $payment->setIsPaid(true);

        $em= $this->getDoctrine()->getManager();

        $em->persist($payment);
        //je déclenche l'insert
        $em->flush();

        if($previousPage == "detail"){
            return $this->redirectToRoute("app_home");
        }else{
            return $this->redirectToRoute("app_home");
        }

    }


}
