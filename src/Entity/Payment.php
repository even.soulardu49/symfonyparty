<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="payments")
     */
    private $userWhoPaid;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="payments")
     */
    private $userWhoNeedToPay;

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPaid;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="payments")
     */
    private $groupId;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserWhoPaid(): ?User
    {
        return $this->userWhoPaid;
    }

    public function setUserWhoPaid(?User $userWhoPaid): self
    {
        $this->userWhoPaid = $userWhoPaid;

        return $this;
    }

    public function getUserWhoNeedToPay(): ?User
    {
        return $this->userWhoNeedToPay;
    }

    public function setUserWhoNeedToPay(?User $userWhoNeedToPay): self
    {
        $this->userWhoNeedToPay = $userWhoNeedToPay;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid): self
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getGroupId(): ?Group
    {
        return $this->groupId;
    }

    public function setGroupId(?Group $groupId): self
    {
        $this->groupId = $groupId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
