<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="groups")
     */
    private $assoUserGroup;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="groupId")
     */
    private $payments;

    public function __construct()
    {
        $this->assoUserGroup = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getAssoUserGroup(): Collection
    {
        return $this->assoUserGroup;
    }

    public function getUsersWithoutTheActual(User $actualUser){
        $userList= array();
        foreach($this->assoUserGroup as $user){
            if($user != $actualUser){
                array_push($userList, $user);
            }
        }
        return $userList;
    }

    public function addAssoUserGroup(User $assoUserGroup): self
    {
        if (!$this->assoUserGroup->contains($assoUserGroup)) {
            $this->assoUserGroup[] = $assoUserGroup;
        }

        return $this;
    }

    public function removeAssoUserGroup(User $assoUserGroup): self
    {
        if ($this->assoUserGroup->contains($assoUserGroup)) {
            $this->assoUserGroup->removeElement($assoUserGroup);
        }

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }



    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setGroupId($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getGroupId() === $this) {
                $payment->setGroupId(null);
            }
        }

        return $this;
    }
}
