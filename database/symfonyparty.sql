-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 13 nov. 2020 à 20:36
-- Version du serveur :  5.7.26
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `symfonyparty`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201021122056', '2020-10-21 12:21:20', 75),
('DoctrineMigrations\\Version20201026193436', '2020-10-26 19:34:55', 407),
('DoctrineMigrations\\Version20201028110012', '2020-10-28 11:00:50', 126),
('DoctrineMigrations\\Version20201031134614', '2020-10-31 13:46:34', 128);

-- --------------------------------------------------------

--
-- Structure de la table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_cost` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `group`
--

INSERT INTO `group` (`id`, `title`, `total_cost`) VALUES
(12, 'Les beau gosses de l\'EPSI', NULL),
(13, 'EPSI Staff', NULL),
(14, 'Les élèves les plus forts', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `group_user`
--

DROP TABLE IF EXISTS `group_user`;
CREATE TABLE IF NOT EXISTS `group_user` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `IDX_A4C98D39FE54D947` (`group_id`),
  KEY `IDX_A4C98D39A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `group_user`
--

INSERT INTO `group_user` (`group_id`, `user_id`) VALUES
(12, 6),
(12, 7),
(12, 8),
(12, 13),
(13, 6),
(13, 7),
(13, 8),
(13, 12),
(13, 13),
(14, 6),
(14, 9),
(14, 10),
(14, 11),
(14, 13);

-- --------------------------------------------------------

--
-- Structure de la table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_who_paid_id` int(11) DEFAULT NULL,
  `user_who_need_to_pay_id` int(11) DEFAULT NULL,
  `group_id_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `date` date NOT NULL,
  `is_paid` tinyint(1) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6D28840D4ECFB10C` (`user_who_paid_id`),
  KEY `IDX_6D28840D671EB03` (`user_who_need_to_pay_id`),
  KEY `IDX_6D28840D2F68B530` (`group_id_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `payment`
--

INSERT INTO `payment` (`id`, `user_who_paid_id`, `user_who_need_to_pay_id`, `group_id_id`, `amount`, `date`, `is_paid`, `title`) VALUES
(15, 8, 6, 12, 12.5, '2020-11-13', 0, 'Petit déjeuner du 10/11'),
(16, 8, 7, 12, 12.5, '2020-11-13', 1, 'Petit déjeuner du 10/11'),
(17, 8, 6, 12, 1.5, '2020-11-13', 0, 'Pause café du 08/11'),
(18, 8, 7, 12, 1.5, '2020-11-13', 1, 'Pause café du 08/11'),
(19, 8, 12, 13, 300, '2020-11-13', 0, 'Hébergement des serveurs'),
(20, 8, 12, 13, 60, '2020-11-13', 1, 'Mise en place de la fibre'),
(21, 12, 6, 13, 33.33, '2020-11-13', 0, 'Usage de MES locaux'),
(22, 12, 7, 13, 33.33, '2020-11-13', 1, 'Usage de MES locaux'),
(23, 12, 8, 13, 33.33, '2020-11-13', 1, 'Usage de MES locaux'),
(24, 13, 6, 14, 1, '2020-11-13', 0, 'Frais de mignonnerie'),
(25, 13, 6, 13, 1, '2020-11-13', 0, 'Frais de mignonnerie'),
(26, 13, 6, 12, 1, '2020-11-13', 0, 'Frais de mignonnerie'),
(27, 6, 7, 12, 7.5, '2020-11-13', 1, 'Chouquettes'),
(28, 6, 8, 12, 7.5, '2020-11-13', 1, 'Chouquettes'),
(29, 6, 12, 13, 300, '2020-11-13', 0, 'Salaire Alan pour 1min de cours'),
(30, 6, 9, 14, 50, '2020-11-13', 1, 'Enseignement du maître'),
(31, 6, 10, 14, 50, '2020-11-13', 1, 'Enseignement du maître'),
(32, 6, 11, 14, 50, '2020-11-13', 1, 'Enseignement du maître');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `first_name`, `last_name`) VALUES
(6, 'alan.ferro@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$Yy9oMDdIRzZ6cGVDdTN3SA$LYLewDax8joNK8LAVnyNUCMvynnAeedHoshmUo8jNDM', 'Alan', 'Ferronnière'),
(7, 'mathiasbraux@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$bGpzdHZnZzJrMlBpb2ZCbA$sJDiUCYRhBLdzRYPeL0JRh5K6+0EOr/TlI7RiBFO2rA', 'Mathias', 'Braux'),
(8, 'fredericreinold@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$LnNDbkwxTzRSTmQueHJQbQ$/aZNhuHTUJ3kce2BzkCEaYZLQbpGFxGhHu7PNo5oLkg', 'Frédéric', 'Reinold'),
(9, 'maloroyan@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$M1VMWUEyaVRFRzdDckcyNw$JGYxssim5mD1yUG1ARUR+UXTvLHo6N77ZMIZFugrPL4', 'Malo', 'Royan'),
(10, 'evensoulard@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$ek1rRTIvNEJvdVJHWDlSdw$l6gl5FDl9pTpOJUvhMj+JPW30GQ7K+do8oCjNffaRM4', 'Even', 'Soulard'),
(11, 'briachillion@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$ZVhoczJvcHd2ZFJmaWJxQw$ycULbyCCN2aIhTMJ68ZL4vO3PilAJO0ai9qRSS4v+6w', 'Briac', 'Hillion'),
(12, 'labigboss@email.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$MWYyZ21FTXl5Vlg1aTVJQw$vnAebaPZXT7o9+AQAwRY3qTb6aNGyp2mNhcrXbzbrHw', 'Alexandrine', 'Grohs'),
(13, 'petitchatmignon@mignon.fr', '[]', '$argon2id$v=19$m=65536,t=4,p=1$ZVliVWNFc0tVZy5GNFo4dw$cQtsIDDOGj/Q0WDXpZMKoheOikrtuZBVR4Yvewqv40U', 'Petit chaton', 'Mignon');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `group_user`
--
ALTER TABLE `group_user`
  ADD CONSTRAINT `FK_A4C98D39A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_A4C98D39FE54D947` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `FK_6D28840D2F68B530` FOREIGN KEY (`group_id_id`) REFERENCES `group` (`id`),
  ADD CONSTRAINT `FK_6D28840D4ECFB10C` FOREIGN KEY (`user_who_paid_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_6D28840D671EB03` FOREIGN KEY (`user_who_need_to_pay_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
